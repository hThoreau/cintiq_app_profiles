# cintiq_app_profiles

## About

This program allows users to create button mapping profiles for Cintiq 13HD, tailored for each application. These profiles are then loaded by the tablet on the fly, as soon as the listed applications come into focus. Currently there are profiles built in for *GIMP*, *Inkscape*, *Krita*, *Photoshop*, *Inkscape*, and a base profile listing key shortcuts and modifier keys that are of common use to all these applications. These are listed, and can be edited, from within the *"wacom_app_profiles"* file.

## Dependencies

This script requires that the packages `xdotool`, `xinput` and `xserver-xorg-input-wacom` are installed.

## Installation

```
mkdir $HOME/bin
cd $HOME/bin
wget https://gitlab.com/hThoreau/cintiq_app_profiles/raw/master/cintiq_app_profiles
chmod +x wacom-application-profiles
cd $HOME/.config/autostart
wget https://gitlab.com/hThoreau/cintiq_app_profiles/raw/master/cintiq_app_profiles.desktop
```
To install Cintiq Synergy and have it loaded upon login, copy *"cintiq-synergy.desktop"* to the *"/$HOME/.config/autostart/"* folder. Also, copy *"wacom-application-profiles"* to the *"/$HOME/bin"* folder. Then, upon the next login it will start automatically, running in the background. Either way, you can always launch it by using the "wacom-application-profiles" command in your terminal, which my come in handy when you wish to have your profiles loaded soon after you've just peformed a modification in your profiles list.

## Modifying Profiles

All you'll ever have to do is modify the lines containing the *"SetCintiq"* function, such as line 18: *'SetCintiq "2" "+super z -super"'*, with the desired button mapping ID and key binding respectively, within double quotes.

## Key Bindings

As for the key binding, it is much more straightfoward: either simply assign a key, such as *"b"*, to indicate a single key press, or precede it with a *"+"*, such as *"+ctrl"* to indicate that whenever you hold the button, it translates into a key being continually pressed. This is particularly useful for modifier keys, which are often used in combination with other keys and also with each other, and also a necessity for performing multikey operations using such modifier keys, such as *Ctrl+Z*. As an example, in the case of *Ctrl+Z*, we could use *'SetCintiq "3" "+ctrl z -ctrl"'* with and added *"-ctrl"* by the end of it to indicate that we with that the button 3 to have assign to it the command to HOLD Ctrl (*+ctrl*), PRESS Z (*z*) and then RELEASE Ctrl afterwards (*-ctrl*).

## Adding new profiles

To add a new application profile, list it under the "case" statement using the following structure, and its application name as presented by the application's header:
```
*application_name* )
	if [ "$LoadedProfile" != "aplication_name" ]; then
	SetButton "TopRadial"	"key_binding"
	...
	SetButton "RightRadial"	"key_binding"
	LoadedProfile="application_name"
	fi;;
```

